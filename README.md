Calculator
===============================

This is a demo React App to compute the sum / multiplication of three numbers.

![Screenshot](http://calculator.softhem.se/screenshot.png)

# [Demo](http://calculator.softhem.se/)

INSTALLATIONS
---------------
### Dev
  * Clone the repository `git clone https://iloveyii@bitbucket.org/iloveyii/calculator.git`.
  * Run nm install `npm install`.
  * Then run npm run command `npm start`.
  
### Prod
  * Point web browser to build/index.html directory or Create a virtual host using [vh](https://github.com/iloveyii/vh) `vh new calculator -p ~/calculator/frontend/web`
  * Browse to [http://calculator.loc](http://calculator.loc) 
  

DIRECTORY STRUCTURE
-------------------

```
public                   contains public assets
src                      contains the source code of the app
    components           contains the React components
App.css                  is the css of the app
App.js                   is the main component of the app
index.js                 is the entry point of the app
```