import React from 'react';


class Value2 extends React.Component {
    constructor(props) {
        super(props);
        this.inputValue = 0;
        this.onChange = this.onChange.bind(this);
        this.state = {value2: 0};

    }

    componentDidMount() {
        this.setState({value2: this.props.value2});
    }

    onChange = (e) => {
        this.setState({value2: e.target.value});
        const {setValue} = this.props;
        setValue({value2: e.target.value});
    };

    render() {
        return (
            <div className="block blue">
                <div className="inner">
                    <label style={{flex: 1}} htmlFor="value2">Value 2:</label>
                    <input style={{flex: 1}} type="text" value={this.state.value2} onKeyUp={this.onChange}
                           onChange={this.onChange}/>
                </div>
            </div>
        )
    }
}

export default Value2;