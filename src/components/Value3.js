import React from 'react';


class Value3 extends React.Component {
    constructor(props) {
        super(props);
        this.inputValue = 0;
        this.onChange = this.onChange.bind(this);
        this.state = {value3: 0};

    }

    componentDidMount() {
        this.setState({value3: this.props.value3});
    }

    onChange = (e) => {
        this.setState({value3: e.target.value});
        const {setValue} = this.props;
        setValue({value3: e.target.value});
    };

    render() {
        return (
            <div className="block blue">
                <div className="inner">
                    <label style={{flex: 1}} htmlFor="value3">Value 3:</label>
                    <input style={{flex: 1}} type="text" value={this.state.value3} onKeyUp={this.onChange}
                           onChange={this.onChange}/>
                </div>
            </div>
        )
    }
}

export default Value3;