import React from 'react';


class Value1 extends React.Component {
    constructor(props) {
        super(props);
        this.inputValue = 0;
        this.onChange = this.onChange.bind(this);
        this.state = {value1: 0};

    }

    componentDidMount() {
        this.setState({value1: this.props.value1});
    }

    onChange = (e) => {
        this.setState({value1: e.target.value})
        const {setValue} = this.props;
        setValue({value1: e.target.value});
    }

    render() {

        return (
            <div className="block blue">
                <div className="inner">
                    <label style={{flex: 1}} htmlFor="value1">Value 1:</label>
                    <input style={{flex: 1}} type="text" value={this.state.value1} onKeyUp={this.onChange}
                           onChange={this.onChange}/>
                </div>
            </div>
        )
    }
}

export default Value1;