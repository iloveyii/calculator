import React from 'react';

class Choice extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(choice) {
        const {setChoice} = this.props;
        setChoice(choice);
    }

    render() {
        const {result, choice} = this.props;

        return (
            <div className="block pink">
                <div className="inner" style={{flexDirection: 'column'}}>
                    <div className="radioDiv">
                        <input type="radio" name="choice" onClick={() => this.onClick('Sum')} onChange={() => {
                        }} checked={choice === 'Sum' ? 'checked' : ''}/>
                        <label htmlFor="choice">Sum</label>
                    </div>

                    <div className="radioDiv">
                        <input type="radio" name="choice" onClick={() => this.onClick('Multiply')} onChange={() => {
                        }} checked={choice === 'Multiply' ? 'checked' : ''}/>
                        <label htmlFor="choice"> Multiply </label>
                    </div>

                    <div className="radioDiv">
                        <div className="result">Result: {result}</div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Choice;