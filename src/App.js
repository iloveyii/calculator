import React, {Component} from 'react';

import Value1 from './components/Value1';
import Value2 from './components/Value2';
import Value3 from './components/Value3';
import Choice from './components/Choice';

import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value1: 0,
            value2: 0,
            value3: 0,
            result: 0,
            choice: 'Sum'
        };
        this.setValue = this.setValue.bind(this);
        this.setChoice = this.setChoice.bind(this);
    }

    componentDidMount() {
        this.setChoice(this.state.choice);
    }

    setValue(val) {
        this.setState(val);
        this.setChoice(this.state.choice);
    }

    setChoice(choice) {
        if (choice === 'Sum') {
            const result = Number(this.state.value1)
                + Number(this.state.value2)
                + Number(this.state.value3);
            const choice = 'Sum';
            this.setState({result, choice});
        }

        if (choice === 'Multiply') {
            const result = Number(this.state.value1)
                * Number(this.state.value2)
                * Number(this.state.value3);
            const choice = 'Multiply';
            this.setState({result, choice});
        }
    }


    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1>Calculator</h1>
                </header>
                <div className="App-body">
                    <Value1 className="calculator blue" value1={this.state.value1} setValue={this.setValue}/>
                    <Value2 className="calculator blue" value2={this.state.value2} setValue={this.setValue}/>
                    <Value3 className="calculator blue" value3={this.state.value3} setValue={this.setValue}/>
                    <Choice result={this.state.result} setChoice={this.setChoice} choice={this.state.choice}/>
                </div>
            </div>
        );
    }
}

export default App;
